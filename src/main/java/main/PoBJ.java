package main;

import java.awt.EventQueue;

import ui.DesktopFrame;

public class PoBJ {

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			DesktopFrame ui = new DesktopFrame();
			ui.show();
		});
	}
}
