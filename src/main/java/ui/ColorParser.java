package ui;

import java.awt.Color;
import java.util.function.Predicate;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import io.vavr.Tuple2;
import io.vavr.collection.List;

public class ColorParser {
	private static final Pattern COLOR_PATTERN = Pattern.compile("\\^([0-9]|x[0-9ABCDEFabcdef]{6})");
	private static final Color[] COLOR_CODES = new Color[] { //
			Color.black, //
			Color.red, //
			Color.green, //
			Color.blue, //
			Color.yellow, //
			Color.magenta, //
			Color.cyan, //
			Color.white, //
			new Color(0.7f, 0.7f, 0.7f), //
			new Color(0.4f, 0.4f, 0.4f) //
	};

	private ColorParser() {
	}

	public static Color string2color(String str) {
		if (str.charAt(0) == '^') {
			if (str.charAt(1) == 'x') {
				int r = Integer.parseInt(str.substring(2, 4), 16);
				int g = Integer.parseInt(str.substring(4, 6), 16);
				int b = Integer.parseInt(str.substring(6, 8), 16);
				return new Color(r, g, b);
			} else {
				return COLOR_CODES[Integer.parseInt(str.substring(1, 2))];
			}
		}
		return Color.white;
	}

	public static String stripColors(String text) {
		return COLOR_PATTERN.matcher(text).replaceAll("");
	}

	private static final Predicate<String> STR_NOT_EMPTY = ((Predicate<String>) String::isEmpty).negate();

	public static List<Tuple2<Color, String>> parseString(String text) {
		final List<MatchResult> matches = List.ofAll(COLOR_PATTERN.matcher(text).results());

		List<Tuple2<Integer, Integer>> boundaries = matches.zipWithIndex().map(
				t2 -> new Tuple2<>(t2._1.start(), matches.size() > t2._2 + 1 ? matches.get(t2._2 + 1).start() : null));
		if (!boundaries.isEmpty() && boundaries.head()._1 != 0) {
			boundaries = boundaries.prepend(new Tuple2<>(0, boundaries.head()._1));
		}

		List<String> list = boundaries.map(t2 -> text.substring(t2._1, t2._2 == null ? text.length() : t2._2));
		if (list.isEmpty()) {
			list = List.of(text);
		}

		return list.filter(STR_NOT_EMPTY).map(str -> {
			if (COLOR_PATTERN.matcher(str).find())
				return new Tuple2<>(string2color(str), stripColors(str));
			return new Tuple2<>(Color.white, str);
		}).filter(t2 -> STR_NOT_EMPTY.test(t2._2));
	}
}
