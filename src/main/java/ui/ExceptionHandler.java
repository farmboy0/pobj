package ui;

public interface ExceptionHandler {
	void handleException(String title, Exception e);
}
