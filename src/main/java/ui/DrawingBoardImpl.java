package ui;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;

import data.Alignment;
import data.FontType;
import data.ImageHandle;
import data.Keyname;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.LinearSeq;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.collection.Stream;
import lua.LuaHandler;
import ui.DrawData.DrawColoredData;
import ui.DrawData.DrawTextureData;
import ui.DrawData.ViewportData;

public class DrawingBoardImpl implements DrawingBoard {

	private final StringCache stringCache;
	private final LuaHandler handler;

	private LinearSeq<DrawData> currentDrawData;

	private int cursorX;
	private int cursorY;
	private int width;
	private int height;

	private boolean updateQueued;

	public DrawingBoardImpl() {
		this.stringCache = new StringCache();
		this.handler = new LuaHandler(this);
		this.currentDrawData = List.empty();
	}

	private Consumer<String> titleConsumer;
	private Consumer<Boolean> showCursorConsumer;

	void init(int width, int height, @Nonnull Consumer<String> titleConsumer, Consumer<Boolean> showCursorConsumer) {
		this.width = width;
		this.height = height;
		this.titleConsumer = titleConsumer;
		this.showCursorConsumer = showCursorConsumer;
		handler.startLaunch(null);
		handler.runMethod("OnInit");
		System.out.println("OnInit done");
		updateQueued = true;
		update();
	}

	private static final ExecutorService UPDATER = Executors.newSingleThreadExecutor();

	private synchronized void update() {
		UPDATER.submit(this::onFrame);
	}

	private LinearSeq<DrawData> drawData;
	private Color drawColor;
	private int currentLayer;
	private int currentSubLayer;
	private float currentZ;

	private void onFrame() {
		updateQueued = false;
		drawData = Stream.empty();
		setDrawLayer(0, 0);
		long start = System.currentTimeMillis();
		handler.runMethod("OnFrame");
		long end = System.currentTimeMillis();
		System.out.println("OnFrame took " + (end - start) + " ms to render.");
		currentDrawData = drawData;
	}

	private float currentZ() {
		return currentZ;
	}

	void exit() {
		handler.runMethod("OnExit");
		updateQueued = true;
		update();
	}

	LinearSeq<DrawData> getCurrentData() {
		return currentDrawData;
	}

	void updateWindowSize(int width, int height) {
		this.width = width;
		this.height = height;
		updateQueued = true;
		update();
	}

	void updateCursor(int cursorX, int cursorY) {
		this.cursorX = cursorX;
		this.cursorY = cursorY;
		if (!updateQueued) {
			updateQueued = true;
			update();
		}
	}

	private Set<Keyname> PRESSED_KEYS = HashSet.empty();

	void keyDown(Keyname key, boolean doubleClick) {
		PRESSED_KEYS = PRESSED_KEYS.add(key);
		handler.keyDown(key, doubleClick);
		updateQueued = true;
		update();
	}

	void keyDown(Keyname key) {
		PRESSED_KEYS = PRESSED_KEYS.add(key);
		handler.keyDown(key);
		updateQueued = true;
		update();
	}

	void keyDown(String key) {
		handler.keyDown(key);
		updateQueued = true;
		update();
	}

	void ctrlKeyDown(String key) {
		handler.ctrlkeyDown(key);
		updateQueued = true;
		update();
	}

	void keyUp(Keyname key) {
		PRESSED_KEYS = PRESSED_KEYS.remove(key);
		handler.keyUp(key);
		updateQueued = true;
		update();
	}

	@Override
	public void loadImage(ImageHandle img) {
		try {
			img.image = ImageIO.read(new File(img.file));
		} catch (IOException e) {
			System.err.println(String.format("%s - %s", img.file, e));
		}
	}

	@Override
	public void unloadImage(ImageHandle img) {
		img.image = null;
	}

	@Override
	public void setWindowTitle(String title) {
		titleConsumer.accept(title);
	}

	@Override
	public Tuple2<Integer, Integer> getCursorPos() {
		return new Tuple2<>(cursorX, cursorY);
	}

	@Override
	public void showCursor(boolean doShow) {
		showCursorConsumer.accept(doShow);
	}

	@Override
	public boolean isKeyDown(Keyname key) {
		return PRESSED_KEYS.contains(key);
	}

	@Override
	public void copyToClipboard(String toCopy) {
		// TODO
	}

	@Override
	public String pasteFromClipboard() {
		try {
			return Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null)
					.getTransferData(DataFlavor.stringFlavor).toString();
		} catch (UnsupportedFlavorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public void initRendering() {
		// nothing to do
	}

	@Override
	public Tuple2<Integer, Integer> getScreenSize() {
		return new Tuple2<>(width, height);
	}

	@Override
	public void setDrawLayer(Integer layer, Integer subLayer) {
		if (layer != null) {
			currentLayer = layer;
			currentSubLayer = 0;
		}
		if (subLayer != null) {
			currentSubLayer = subLayer;
		}
		currentZ = currentLayer + currentSubLayer / 100.0f;
	}

	@Override
	public void setViewport(int x, int y, int width, int height) {
		drawData = drawData.append(new ViewportData(x, y, width, height));
	}

	@Override
	public void setDrawColor(float r, float g, float b, float a) {
		drawColor = new Color(r, g, b, a);
	}

	@Override
	public void setDrawColor(String color) {
		drawColor = ColorParser.string2color(color);
	}

	@Override
	public void drawColor(float x, float y, float width, float height) {
		drawData = drawData.append(
				new DrawColoredData(drawColor, x, y, x + width, y, x + width, y + height, x, y + height, currentZ()));
	}

	@Override
	public void drawColor(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		drawData = drawData.append(new DrawColoredData(drawColor, x1, y1, x2, y2, x3, y3, x4, y4, currentZ()));
	}

	@Override
	public void drawImage(ImageHandle img, float x, float y, float w, float h, float u1, float v1, float u2, float v2) {
		drawData = drawData.append(new DrawTextureData(img.image, //
				x, y, x + w, y, x + w, y + h, x, y + h, //
				u1, v1, u2, v1, u2, v2, u1, v2, currentZ(), img.file));
	}

	@Override
	public void drawImage(ImageHandle img, float x1, float y1, float x2, float y2, float x3, float y3, float x4,
			float y4, float u1, float v1, float u2, float v2, float u3, float v3, float u4, float v4) {

		drawData = drawData.append(new DrawTextureData(img.image, //
				x1, y1, x2, y2, x3, y3, x4, y4, //
				u1, v1, u2, v2, u3, v3, u4, v4, currentZ(), img.file));
	}

	@Override
	public void drawString(float x, float y, int height, Alignment align, FontType font, String text) {
		final BufferedImage image = stringCache.textureFor(font, height, text);
		if (image == null) {
			return;
		}
		int w = image.getWidth();
		int h = image.getHeight();
		int startX = align.computeStart((int) x, width, w);

		drawData = drawData.append(new DrawTextureData(image, //
				startX, y, startX + w, y, startX + w, y + h, startX, y + h, //
				0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, currentZ(), text));
	}

	@Override
	public int drawStringWidth(int height, FontType font, String text) {
		return stringCache.stringWidthFor(font, height, text);
	}

	@Override
	public int drawStringCursorIndex(int height, FontType font, String text, int cursorX, int cursorY) {
		return Stream.of(text.split("\n")) //
				.map(s -> stringCache.stringWidthFor(font, height, s)) //
				.max() //
				.getOrElse(0);
	}

	@Override
	public String stripColorCodes(String text) {
		return ColorParser.stripColors(text);
	}
}
