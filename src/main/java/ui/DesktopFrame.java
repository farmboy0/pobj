package ui;

import java.io.File;

import javax.annotation.Nonnull;

import com.jogamp.newt.event.WindowAdapter;
import com.jogamp.newt.event.WindowEvent;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.FPSAnimator;

public class DesktopFrame implements ExceptionHandler {
	private GLWindow window;

	private FPSAnimator animator;

	public DesktopFrame() {
		loadSettings();
		initFrame();
	}

	public void show() {
		this.window.setVisible(true);
	}

	private void initFrame() {
		GLProfile glProfile = GLProfile.get(GLProfile.GL4);
		GLCapabilities glCapabilities = new GLCapabilities(glProfile);

		this.window = GLWindow.create(glCapabilities);
		this.window.setTitle("PoB");
		this.window.setSize(800, 600);
		this.window.addGLEventListener(getEventHandler());
		this.window.addWindowListener(new DesktopWindowAdapter());

		this.animator = new FPSAnimator(window, 15);
		this.animator.start();
	}

	private OpenGLEventHandler renderer;

	private OpenGLEventHandler getEventHandler() {
		if (renderer == null) {
			renderer = new OpenGLEventHandler();
		}
		return renderer;
	}

	private void quit() {
		saveSettings();
		System.exit(0);
	}

	@Override
	public void handleException(String title, Exception e) {
		// TODO Auto-generated method stub

	}

	private void loadSettings() {
	}

	private void saveSettings() {
	}

	@Nonnull
	private File getConfigPath() {
		File parent = null;
		if (System.getenv("XDG_CONFIG_DIR") != null) {
			parent = new File(System.getenv("XDG_CONFIG_DIR"));
		} else if (System.getProperty("user.home") != null) {
			parent = new File(System.getProperty("user.home"), ".config");
		} else {
			parent = new File(System.getProperty("user.dir"));
		}
		return new File(parent, "pobj");
	}

	private final class DesktopWindowAdapter extends WindowAdapter {
		@Override
		public void windowDestroyed(WindowEvent e) {
			quit();
		}
	}
}
