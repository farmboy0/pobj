package ui;

import java.awt.Color;
import java.awt.image.BufferedImage;

abstract class DrawData {

	static final class ViewportData extends DrawData {
		public final int x;
		public final int y;
		public final int width;
		public final int height;

		ViewportData(int x, int y, int width, int height) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		@Override
		public String toString() {
			return String.format("Viewport[x=%s, y=%s, width=%s, height=%s]", x, y, width, height);
		}
	}

	static final class DrawColoredData extends DrawData {
		public final Color c;
		public final float x1;
		public final float y1;
		public final float x2;
		public final float y2;
		public final float x3;
		public final float y3;
		public final float x4;
		public final float y4;

		public final float z;

		DrawColoredData(Color c, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float z) {
			this.c = c;

			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.x3 = x3;
			this.y3 = y3;
			this.x4 = x4;
			this.y4 = y4;

			this.z = z;
		}

		@Override
		public String toString() {
			return String.format("DrawColor[c=%s, (%s, %s), (%s, %s), (%s, %s), (%s, %s), z=%s]", c, x1, y1, x2, y2, x3, y3, x4, y4, z);
		}
	}

	static final class DrawTextureData extends DrawData {
		public final BufferedImage img;

		public final float x1;
		public final float y1;
		public final float x2;
		public final float y2;
		public final float x3;
		public final float y3;
		public final float x4;
		public final float y4;

		public final float u1;
		public final float v1;
		public final float u2;
		public final float v2;
		public final float u3;
		public final float v3;
		public final float u4;
		public final float v4;

		public final float z;

		private String text;

		DrawTextureData(BufferedImage img, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float u1, float v1,
			float u2, float v2, float u3, float v3, float u4, float v4, float z, String text) {

			this.img = img;

			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.x3 = x3;
			this.y3 = y3;
			this.x4 = x4;
			this.y4 = y4;

			this.u1 = u1;
			this.v1 = v1;
			this.u2 = u2;
			this.v2 = v2;
			this.u3 = u3;
			this.v3 = v3;
			this.u4 = u4;
			this.v4 = v4;

			this.z = z;
			this.text = text;
		}

		@Override
		public String toString() {
			return String.format("DrawTexture [%s (%s, %s), (%s, %s), (%s, %s), (%s, %s), (%s, %s), (%s, %s), (%s, %s), (%s, %s), z=%s]", text, x1,
				y1, x2, y2, x3, y3, x4, y4, u1, v1, u2, v2, u3, v3, u4, v4, z);
		}
	}
}
