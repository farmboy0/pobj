package ui;

import static com.jogamp.opengl.GL.GL_DONT_CARE;
import static com.jogamp.opengl.GL2ES2.GL_DEBUG_SEVERITY_HIGH;
import static com.jogamp.opengl.GL2ES2.GL_DEBUG_SEVERITY_MEDIUM;

import java.awt.image.BufferedImage;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.vavr.Tuple2;
import io.vavr.collection.Seq;

import com.jogamp.common.nio.Buffers;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL2ES3;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLDebugListener;
import com.jogamp.opengl.GLDebugMessage;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.util.GLBuffers;
import com.jogamp.opengl.util.glsl.ShaderCode;
import com.jogamp.opengl.util.glsl.ShaderProgram;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import glm.Glm;
import glm.vec._2.Vec2;
import glm.vec._3.Vec3;
import glm.vec._4.Vec4;
import ui.DrawData.DrawColoredData;
import ui.DrawData.DrawTextureData;
import ui.DrawData.ViewportData;

public class OpenGLEventHandler implements GLEventListener, GLDebugListener {
	private static final int COLOR_FLOAT_COUNT = 4 * (3 + 4);
	private static final int TEXTURE_FLOAT_COUNT = 4 * (3 + 2);
	private static final int UNIFORM_FLOAT_COUNT = 16;

	private static final FloatBuffer CLEAR_COLOR = Buffers.newDirectFloatBuffer(4).put(0, 0f).put(1, 0f).put(2, 0f).put(3, 1f);
	private static final FloatBuffer CLEAR_DEPTH = Buffers.newDirectFloatBuffer(1).put(0, -100f);

	private final DrawingBoardImpl board;

	public OpenGLEventHandler() {
		board = new DrawingBoardImpl();
	}

	private int colorProgramId, textureProgramId;
	private IntBuffer vaoId, samplerId;

	@Override
	public void init(GLAutoDrawable drawable) {
		GLWindow window = (GLWindow) drawable;

		final GL4 gl = window.getGL().getGL4();
		//		initDebug(gl);

		vaoId = Buffers.newDirectIntBuffer(2);
		gl.glGenVertexArrays(2, vaoId);
		gl.glBindVertexArray(vaoId.get(0));

		// Position vertex attribute
		gl.glVertexArrayAttribBinding(vaoId.get(0), 0, 0);
		gl.glVertexArrayAttribFormat(vaoId.get(0), 0, 3, GL.GL_FLOAT, false, 0);
		gl.glEnableVertexArrayAttrib(vaoId.get(0), 0);
		// Color vertex attribute
		gl.glVertexArrayAttribBinding(vaoId.get(0), 1, 0);
		gl.glVertexArrayAttribFormat(vaoId.get(0), 1, 4, GL.GL_FLOAT, false, Vec3.SIZE);
		gl.glEnableVertexArrayAttrib(vaoId.get(0), 1);

		gl.glBindVertexArray(0);
		gl.glBindVertexArray(vaoId.get(1));

		// Position vertex attribute
		gl.glVertexArrayAttribBinding(vaoId.get(1), 0, 0);
		gl.glVertexArrayAttribFormat(vaoId.get(1), 0, 3, GL.GL_FLOAT, false, 0);
		gl.glEnableVertexArrayAttrib(vaoId.get(1), 0);
		// TexCoord vertex attribute
		gl.glVertexArrayAttribBinding(vaoId.get(1), 1, 0);
		gl.glVertexArrayAttribFormat(vaoId.get(1), 1, 2, GL.GL_FLOAT, false, Vec3.SIZE);
		gl.glEnableVertexArrayAttrib(vaoId.get(1), 1);

		gl.glBindVertexArray(0);

		colorProgramId = createShaderProgram(gl, "color");
		textureProgramId = createShaderProgram(gl, "texture");

		samplerId = GLBuffers.newDirectIntBuffer(1);
		gl.glGenSamplers(1, samplerId);
		gl.glBindSampler(0, samplerId.get(0));

		gl.glSamplerParameteri(samplerId.get(0), GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
		gl.glSamplerParameteri(samplerId.get(0), GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);

		//		gl.glSamplerParameteri(samplerId.get(0), GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
		//		gl.glSamplerParameteri(samplerId.get(0), GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);

		gl.glDepthMask(true);
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glDepthFunc(GL.GL_GEQUAL);
		gl.glEnable(GL.GL_BLEND);
		gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

		board.init(window.getSurfaceWidth(), window.getSurfaceHeight(), window::setTitle, window::setPointerVisible);

		final InputHandler i = new InputHandler(board);
		window.addKeyListener(i);
		window.addMouseListener(i);
	}

	private int createShaderProgram(GL4 gl, String name) {
		ShaderCode vertShader = ShaderCode.create(gl, GL2ES2.GL_VERTEX_SHADER, this.getClass(), "shader", null, name, null, null, true);
		ShaderCode fragShader = ShaderCode.create(gl, GL2ES2.GL_FRAGMENT_SHADER, this.getClass(), "shader", null, name, null, null, true);

		ShaderProgram shaderProgram = new ShaderProgram();

		shaderProgram.add(vertShader);
		shaderProgram.add(fragShader);

		shaderProgram.init(gl);
		shaderProgram.link(gl, System.err);

		return shaderProgram.program();
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		//		long start = System.currentTimeMillis();

		currentIds.clear();

		final GL4 gl = drawable.getGL().getGL4();
		gl.glClearBufferfv(GL2ES3.GL_COLOR, 0, CLEAR_COLOR);
		gl.glClearBufferfv(GL2ES3.GL_DEPTH, 0, CLEAR_DEPTH);

		render(gl, board.getCurrentData());

		//		long end = System.currentTimeMillis();
		//		System.out.println("frame took " + (end - start) + " ms to render.");
	}

	private final Map<Integer, List<Integer>> bufferIds = new HashMap<>();
	private final Map<Integer, Integer> currentIds = new HashMap<>();

	private int nextBuffer(GL4 gl, int floatCount, int bindTarget) {
		final int currentId = currentIds.computeIfAbsent(floatCount, x -> 0);
		final List<Integer> idsForCount = bufferIds.computeIfAbsent(floatCount, x -> new ArrayList<>());
		currentIds.put(floatCount, currentId + 1);
		if (currentId >= idsForCount.size()) {
			final IntBuffer vboId = Buffers.newDirectIntBuffer(1);
			gl.glGenBuffers(1, vboId);
			idsForCount.add(vboId.get(0));
		}
		final int bufferId = idsForCount.get(currentId);
		gl.glBindBuffer(bindTarget, bufferId);
		return bufferId;
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		board.updateWindowSize(width, height);
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		board.exit();
		System.exit(0);
	}

	private void initDebug(GL4 gl) {
		gl.getContext().addGLDebugListener(this);
		gl.glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, null, false);
		gl.glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_HIGH, 0, null, true);
		gl.glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_MEDIUM, 0, null, true);
	}

	@Override
	public void messageSent(GLDebugMessage event) {
		System.out.println("[GLDEBUG] " + event);
	}

	private final java.util.Map<BufferedImage, Texture> TEXTURE_CACHE = new HashMap<>();

	private final FloatBuffer colorDrawData = FloatBuffer.allocate(COLOR_FLOAT_COUNT);
	private final FloatBuffer textureDrawData = FloatBuffer.allocate(TEXTURE_FLOAT_COUNT);
	private final FloatBuffer uniformBufferData = FloatBuffer.allocate(UNIFORM_FLOAT_COUNT);

	private void render(GL4 gl, Seq<DrawData> currentDataMap) {
		currentDataMap.forEach(data -> render(gl, data));
	}

	private void render(GL4 gl, DrawData data) {
		if (data instanceof ViewportData) {
			ViewportData vpData = (ViewportData) data;

			final Tuple2<Integer, Integer> screenSize = board.getScreenSize();
			gl.glViewport(vpData.x, screenSize._2 - vpData.y - vpData.height, vpData.width, vpData.height);

			Glm.ortho_(0, vpData.width, vpData.height, 0, 9999, -9999).toDfb(uniformBufferData);

			int bufferId = nextBuffer(gl, UNIFORM_FLOAT_COUNT, GL2ES3.GL_UNIFORM_BUFFER);
			gl.glBufferData(GL2ES3.GL_UNIFORM_BUFFER, uniformBufferData.limit() * Float.BYTES, uniformBufferData.rewind(), GL.GL_STATIC_DRAW);
			gl.glBindBuffer(GL2ES3.GL_UNIFORM_BUFFER, 0);
			gl.glBindBufferBase(GL2ES3.GL_UNIFORM_BUFFER, 1, bufferId);
		} else if (data instanceof DrawColoredData) {
			DrawColoredData tData = (DrawColoredData) data;

			colorDrawData.rewind();
			colorDrawData.put(tData.x1).put(tData.y1).put(tData.z).put(tData.c.getRGBComponents(null));
			colorDrawData.put(tData.x2).put(tData.y2).put(tData.z).put(tData.c.getRGBComponents(null));
			colorDrawData.put(tData.x3).put(tData.y3).put(tData.z).put(tData.c.getRGBComponents(null));
			colorDrawData.put(tData.x4).put(tData.y4).put(tData.z).put(tData.c.getRGBComponents(null));
			int bufferId = nextBuffer(gl, COLOR_FLOAT_COUNT, GL.GL_ARRAY_BUFFER);
			gl.glBufferData(GL.GL_ARRAY_BUFFER, colorDrawData.limit() * Float.BYTES, colorDrawData.rewind(), GL.GL_STATIC_DRAW);
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

			gl.glUseProgram(colorProgramId);
			gl.glBindVertexArray(vaoId.get(0));
			gl.glBindVertexBuffer(0, bufferId, 0, Vec3.SIZE + Vec4.SIZE);
			gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4);
		} else if (data instanceof DrawTextureData) {
			DrawTextureData tData = (DrawTextureData) data;
			Texture texture = TEXTURE_CACHE.computeIfAbsent(tData.img, img -> createTexture(gl, img));
			if (texture == null) {
				return;
			}
			texture.enable(gl);
			texture.bind(gl);

			textureDrawData.rewind();
			textureDrawData.put(tData.x1).put(tData.y1).put(tData.z).put(tData.u1).put(tData.v1);
			textureDrawData.put(tData.x2).put(tData.y2).put(tData.z).put(tData.u2).put(tData.v2);
			textureDrawData.put(tData.x3).put(tData.y3).put(tData.z).put(tData.u3).put(tData.v3);
			textureDrawData.put(tData.x4).put(tData.y4).put(tData.z).put(tData.u4).put(tData.v4);
			int bufferId = nextBuffer(gl, TEXTURE_FLOAT_COUNT, GL.GL_ARRAY_BUFFER);
			gl.glBufferData(GL.GL_ARRAY_BUFFER, textureDrawData.limit() * Float.BYTES, textureDrawData.rewind(), GL.GL_STATIC_DRAW);
			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);

			gl.glUseProgram(textureProgramId);
			gl.glBindVertexArray(vaoId.get(1));
			gl.glBindVertexBuffer(0, bufferId, 0, Vec3.SIZE + Vec2.SIZE);
			gl.glBindTextureUnit(0, texture.getTextureObject());
			gl.glDrawArrays(GL.GL_TRIANGLE_FAN, 0, 4);
		}
	}

	private Texture createTexture(GL4 gl, BufferedImage img) {
		return AWTTextureIO.newTexture(gl.getGLProfile(), img, false);
	}

	private void checkError(GL gl) {

		int error = gl.glGetError();
		if (error != GL.GL_NO_ERROR) {
			String errorString;
			switch (error) {
				case GL.GL_INVALID_ENUM:
					errorString = "GL_INVALID_ENUM";
					break;
				case GL.GL_INVALID_VALUE:
					errorString = "GL_INVALID_VALUE";
					break;
				case GL.GL_INVALID_OPERATION:
					errorString = "GL_INVALID_OPERATION";
					break;
				case GL.GL_INVALID_FRAMEBUFFER_OPERATION:
					errorString = "GL_INVALID_FRAMEBUFFER_OPERATION";
					break;
				case GL.GL_OUT_OF_MEMORY:
					errorString = "GL_OUT_OF_MEMORY";
					break;
				default:
					errorString = "UNKNOWN";
					break;
			}
			System.err.println("[GL ERROR] " + errorString);
		}
	}
}
