package ui;

import static java.awt.RenderingHints.KEY_RENDERING;
import static java.awt.RenderingHints.KEY_TEXT_ANTIALIASING;
import static java.awt.RenderingHints.VALUE_RENDER_QUALITY;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import javax.annotation.CheckForNull;

import data.FontType;
import io.vavr.Tuple2;
import io.vavr.Tuple3;
import io.vavr.Tuple4;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;

public class StringCache {
	private static final Color TRANSPARENT = new Color(1f, 1f, 1f, 0f);

	private Map<Tuple3<FontType, Integer, String>, BufferedImage> cache = HashMap.empty();

	@CheckForNull
	public BufferedImage textureFor(FontType font, int height, String text) {
		final Tuple3<FontType, Integer, String> key = new Tuple3<>(font, height, text);
		if (!cache.containsKey(key)) {
			cache = cache.put(key, create(font, height, text));
		}
		return cache.getOrElse(key, null);
	}

	public int stringWidthFor(FontType font, int height, String text) {
		final BufferedImage t = textureFor(font, height, text);
		return t == null ? 0 : t.getWidth();
	}

	private static final FontRenderContext CONTEXT = new FontRenderContext(new AffineTransform(), true, true);

	private BufferedImage create(FontType fontType, int height, String text) {
		if (text == null || text.isEmpty())
			return null;

		final Font font = fontType.getFont().deriveFont((float) height);

		int imgWidth = 0, imgHeight = 0, startY = 0;
		Seq<Tuple4<Integer, Integer, Color, String>> parts = List.empty();

		final String[] lines = text.split("\n");
		for (String line : lines) {
			final Seq<Tuple2<Color, String>> lineParts = ColorParser.parseString(line);
			final Seq<Tuple2<String, TextLayout>> layouts = lineParts.map(Tuple2::_2) //
					.map(str -> new Tuple2<>(str, new TextLayout(str, font, CONTEXT)));
			final Seq<Float> advances = layouts.map(Tuple2::_2).map(TextLayout::getAdvance);
			final Seq<Float> startingXMap = advances //
					.map(advances::indexOf) //
					.map(idx -> advances.subSequence(0, idx).sum().floatValue());

			float lineWidth = advances.sum().floatValue();
			float lineHeight = layouts.map(Tuple2::_2).map(tl -> tl.getAscent() + tl.getDescent() + tl.getLeading())
					.max().getOrElse((float) height);
			float maxLineAscent = layouts.map(Tuple2::_2).map(TextLayout::getAscent).max().getOrElse((float) height);

			for (int i = 0; i < lineParts.size(); i++) {
				Tuple2<Color, String> t2 = lineParts.get(i);
				int startX = startingXMap.get(i).intValue();
				parts = parts.append(new Tuple4<>(startX, startY + (int) maxLineAscent, t2._1, t2._2));
			}
			startY += lineHeight;
			imgHeight += lineHeight;
			imgWidth = Math.max(imgWidth, (int) lineWidth);
		}

		final BufferedImage img = new BufferedImage(2 + imgWidth, 2 + imgHeight, TYPE_INT_ARGB);
		final Graphics2D g2d = (Graphics2D) img.getGraphics();
		g2d.setRenderingHint(KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g2d.setRenderingHint(KEY_RENDERING, VALUE_RENDER_QUALITY);
		g2d.setBackground(TRANSPARENT);
		g2d.setFont(font);

		g2d.clearRect(0, 0, img.getWidth(), img.getHeight());
		parts.forEach(t -> {
			g2d.setColor(t._3);
			g2d.drawString(t._4, t._1, t._2);
		});

		return img;
	}
}
