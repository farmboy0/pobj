package ui;

import data.Alignment;
import data.FontType;
import data.ImageHandle;
import data.Keyname;
import io.vavr.Tuple2;

public interface DrawingBoard {
	void loadImage(ImageHandle img);

	void unloadImage(ImageHandle img);

	void setWindowTitle(String title);

	Tuple2<Integer, Integer> getCursorPos();

	void showCursor(boolean doShow);

	boolean isKeyDown(Keyname key);

	void copyToClipboard(String toCopy);

	String pasteFromClipboard();

	void initRendering();

	Tuple2<Integer, Integer> getScreenSize();

	void setDrawLayer(Integer layer, Integer subLayer);

	void setViewport(int x, int y, int width, int height);

	void setDrawColor(String color);

	void setDrawColor(float r, float g, float b, float a);

	void drawColor(float x, float y, float width, float height);

	void drawColor(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);

	void drawImage(ImageHandle img, float x, float y, float width, float height, float u1, float v1, float u2, float v2);

	void drawImage(ImageHandle img, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float u1, float v1, float u2,
		float v2, float u3, float v3, float u4, float v4);

	void drawString(float x, float y, int height, Alignment align, FontType font, String text);

	int drawStringWidth(int height, FontType font, String text);

	int drawStringCursorIndex(int height, FontType font, String text, int cursorX, int cursorY);

	String stripColorCodes(String text);
}
