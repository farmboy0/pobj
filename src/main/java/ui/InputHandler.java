package ui;

import static com.jogamp.newt.event.KeyEvent.isPrintableKey;
import static data.Keyname.fromEvent;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.event.MouseEvent;
import com.jogamp.newt.event.MouseListener;

import data.Keyname;

public class InputHandler implements KeyListener, MouseListener {

	private final DrawingBoardImpl board;

	public InputHandler(DrawingBoardImpl board) {
		this.board = board;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		fromEvent(e).peek(this::keyPressed)
			.onEmpty(() -> keyPressedOther(e));
	}

	private void keyPressed(Keyname key) {
		System.out.println("Key pressed: " + key);
		board.keyDown(key);
	}

	private void keyPressedOther(KeyEvent e) {
		if (isPrintableKey(e.getKeyCode(), false)) {
			String charString = Character.toString((char) e.getKeySymbol());
			if (!e.isShiftDown()) {
				charString = charString.toLowerCase();
			}
			if (e.isControlDown())
				board.ctrlKeyDown(charString);
			else
				board.keyDown(charString);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		fromEvent(e).peek(board::keyUp);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// unused
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// unused
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// unused
	}

	@Override
	public void mousePressed(MouseEvent e) {
		fromEvent(e).peek(key -> mousePressed(key, e.getClickCount() > 1));
	}

	private void mousePressed(Keyname key, boolean doubleClick) {
		board.keyDown(key, doubleClick);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		fromEvent(e).peek(board::keyUp);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		board.updateCursor(e.getX(), e.getY());
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		board.updateCursor(e.getX(), e.getY());
	}

	@Override
	public void mouseWheelMoved(MouseEvent e) {
		float yRot = e.getRotation()[1];
		if (yRot > 0.0f) {
			board.keyUp(Keyname.WHEELUP);
		} else {
			board.keyUp(Keyname.WHEELDOWN);
		}
	}
}
