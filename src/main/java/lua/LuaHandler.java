package lua;

import static org.luaj.vm2.LuaValue.valueOf;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.jse.JsePlatform;

import data.Keyname;
import luaj.BaseLibMissing;
import luaj.BitLib;
import ui.DrawingBoard;

public class LuaHandler {
	private Globals globals;
	private LuaUtility general;
	private LuaConsole con;
	private LuaModule module;
	private LuaPaths paths;
	private LuaSubscript subscript;
	private LuaUI ui;
	private LuaCallbacks cb;
	private LuaImages images;
	private LuaRendering render;
	private LuaFileSearch fileSearch;

	public LuaHandler(DrawingBoard board) {
		globals = JsePlatform.standardGlobals();
		globals.load(new BaseLibMissing());
		globals.load(new BitLib());
		globals.load(new LuaCurl());

		general = new LuaUtility();
		general.register(globals);

		con = new LuaConsole();
		con.register(globals);

		module = new LuaModule();
		module.register(globals);

		paths = new LuaPaths();
		paths.register(globals);

		subscript = new LuaSubscript();
		subscript.register(globals);

		ui = new LuaUI(board);
		ui.register(globals);

		cb = new LuaCallbacks();
		cb.register(globals);

		fileSearch = new LuaFileSearch();
		fileSearch.register(globals);

		images = new LuaImages(board);
		images.register(globals);

		render = new LuaRendering(board);
		render.register(globals);
	}

	public void startLaunch(String buildUrl) {
		Varargs luaUrl = buildUrl == null ? LuaValue.NIL : LuaValue.valueOf(buildUrl);
		LuaValue arg = LuaValue.tableOf(luaUrl, 1);
		arg.set("n", 1);
		globals.set("arg", arg);
		LuaValue chunk = module.loadModule("Launch");
		chunk.invoke(luaUrl);
	}

	public void runMethod(String name) {
		cb.mainObject.invokemethod(name);
	}

	public void keyDown(Keyname button) {
		keyDown(button, false);
	}

	public void keyDown(Keyname button, boolean doubleClick) {
		cb.mainObject.invokemethod("OnKeyDown", new LuaValue[] { valueOf(button.name()), valueOf(doubleClick) });
	}

	public void keyDown(String button) {
		cb.mainObject.invokemethod("OnChar", new LuaValue[] { valueOf(button), });
	}

	public void ctrlkeyDown(String button) {
		cb.mainObject.invokemethod("OnKeyDown", new LuaValue[] { valueOf(button), valueOf(false) });
	}

	public void keyUp(Keyname button) {
		cb.mainObject.invokemethod("OnKeyUp", new LuaValue[] { valueOf(button.name()), valueOf(false) });
	}
}
