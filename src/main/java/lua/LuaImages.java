package lua;

import java.util.concurrent.atomic.AtomicInteger;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

import data.ImageHandle;
import data.ImageMode;
import ui.DrawingBoard;

class LuaImages {
	private AtomicInteger handleId;
	private LuaTable metatable;

	private DrawingBoard board;

	LuaImages(DrawingBoard board) {
		this.board = board;
		handleId = new AtomicInteger(1);
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("NewImageHandle", new NewImageHandle());

		final LuaTable img = new LuaTable();
		img.set("Load", new Load());
		img.set("Unload", new Unload());
		img.set("IsValid", new IsValid());
		img.set("IsLoading", new IsLoading());
		img.set("SetLoadingPriority", new SetLoadingPriority());
		img.set("ImageSize", new ImageSize());

		metatable = new LuaTable();
		metatable.set(LuaValue.INDEX, img);
		metatable.set("__gc", new GarbageCollector());
	}

	private class NewImageHandle extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			ImageHandle h = new ImageHandle();
			h.id = handleId.getAndAdd(1);
			h.file = null;
			return LuaValue.userdataOf(h, metatable);
		}
	}

	private class GarbageCollector extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class Load extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			ImageHandle img = (ImageHandle) args.checkuserdata(1, ImageHandle.class);
			img.file = args.checkjstring(2);
			LuaValue mode = args.arg(3);
			if (!mode.isnil()) {
				img.mode = ImageMode.valueOf(mode.checkjstring());
			}
			board.loadImage(img);
			return LuaValue.NONE;
		}
	}

	private class Unload extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			ImageHandle img = (ImageHandle) args.checkuserdata(1, ImageHandle.class);
			board.unloadImage(img);
			return LuaValue.NONE;
		}
	}

	private class IsValid extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			ImageHandle img = (ImageHandle) args.checkuserdata(1, ImageHandle.class);
			return LuaValue.valueOf(img.image != null);
		}
	}

	private class IsLoading extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			ImageHandle img = (ImageHandle) args.checkuserdata(1, ImageHandle.class);
			return LuaValue.valueOf(img.file != null && img.image == null);
		}
	}

	private class SetLoadingPriority extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class ImageSize extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			ImageHandle img = (ImageHandle) args.checkuserdata(1, ImageHandle.class);
			if (img.image == null) {
				return varargsOf(valueOf(0), valueOf(0));
			}
			return varargsOf(valueOf(img.image.getWidth()), valueOf(img.image.getHeight()));
		}
	}
}
