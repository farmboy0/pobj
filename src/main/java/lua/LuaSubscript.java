package lua;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

class LuaSubscript {
	LuaSubscript() {
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("LaunchSubScript", new LaunchSubScript());
		_g.set("AbortSubScript", new AbortSubScript());
		_g.set("IsSubScriptRunning", new IsSubScriptRunning());
	}

	private class LaunchSubScript extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class AbortSubScript extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class IsSubScriptRunning extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}
}
