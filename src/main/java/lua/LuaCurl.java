package lua;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

import data.EasyCurl;

public class LuaCurl extends TwoArgFunction {
	private static final String CURL_OPT_ACCEPT_ENCODING = "__CURL__OPT__ACCEPT__ENCODING__";
	private static final String CURL_OPT_COOKIE = "__CURL__OPT__COOKIE__";
	private static final String CURL_OPT_POST = "__CURL__OPT__POST__";
	private static final String CURL_OPT_POSTFIELDS = "__CURL__OPT__POSTFIELDS__";
	private static final String CURL_OPT_PROXY = "__CURL__OPT__PROXY__";
	private static final String CURL_OPT_USERAGENT = "__CURL__OPT__USERAGENT__";

	private static final String CURL_INFO_SIZE_DOWNLOAD = "__INFO__SIZE__DOWNLOAD__";
	private static final String CURL_INFO_REDIRECT_URL = "__INFO__REDIRECT_URL__";
	private static final String CURL_INFO_RESPONSE_CODE = "__INFO__RESPONSE__CODE__";

	private LuaTable metatable;

	@Override
	public LuaValue call(LuaValue modname, LuaValue env) {
		final LuaTable easy = new LuaTable();
		easy.set("setopt", new setopt());
		easy.set("setopt_url", new setopt_url());
		easy.set("setopt_writefunction", new setopt_writefunction());
		easy.set("getinfo", new getinfo());
		easy.set("perform", new perform());
		easy.set("close", new close());

		metatable = new LuaTable();
		metatable.set(LuaValue.INDEX, easy);

		final LuaTable curl = new LuaTable();
		curl.set("easy", new easy());
		curl.set("OPT_ACCEPT_ENCODING", CURL_OPT_ACCEPT_ENCODING);
		curl.set("OPT_COOKIE", CURL_OPT_COOKIE);
		curl.set("OPT_POST", CURL_OPT_POST);
		curl.set("OPT_POSTFIELDS", CURL_OPT_POSTFIELDS);
		curl.set("OPT_PROXY", CURL_OPT_PROXY);
		curl.set("OPT_USERAGENT", CURL_OPT_USERAGENT);
		curl.set("INFO_SIZE_DOWNLOAD", CURL_INFO_SIZE_DOWNLOAD);
		curl.set("INFO_REDIRECT_URL", CURL_INFO_REDIRECT_URL);
		curl.set("INFO_RESPONSE_CODE", CURL_INFO_RESPONSE_CODE);

		env.set("lcurl.safe", curl);
		env.get("package").get("loaded").set("lcurl.safe", curl);

		return curl;
	}

	private class easy extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			EasyCurl easy = new EasyCurl();
			return LuaValue.userdataOf(easy, metatable);
		}
	}

	private class setopt extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class setopt_url extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class setopt_writefunction extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class getinfo extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class perform extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class close extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

}
