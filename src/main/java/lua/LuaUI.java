package lua;

import io.vavr.Tuple2;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

import data.Keyname;
import ui.DrawingBoard;

class LuaUI {
	private DrawingBoard board;

	LuaUI(DrawingBoard board) {
		this.board = board;
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("SetWindowTitle", new SetWindowTitle());
		_g.set("GetCursorPos", new GetCursorPos());
		_g.set("ShowCursor", new ShowCursor());
		_g.set("IsKeyDown", new IsKeyDown());
		_g.set("Copy", new Copy());
		_g.set("Paste", new Paste());
	}

	private class SetWindowTitle extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			board.setWindowTitle(args.checkjstring(1));
			return LuaValue.NONE;
		}
	}

	private class GetCursorPos extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			Tuple2<Integer, Integer> pos = board.getCursorPos();
			return varargsOf(valueOf(pos._1), valueOf(pos._2));
		}
	}

	private class ShowCursor extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			boolean enable = args.checkboolean(1);
			board.showCursor(enable);
			return LuaValue.NONE;
		}
	}

	private class IsKeyDown extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			Keyname name = Keyname.valueOf(args.checkjstring(1));
			return valueOf(board.isKeyDown(name));
		}
	}

	private class Copy extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			String toCopy = args.checkjstring(1);
			board.copyToClipboard(toCopy);
			return LuaValue.NONE;
		}
	}

	private class Paste extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return valueOf(board.pasteFromClipboard());
		}
	}
}
