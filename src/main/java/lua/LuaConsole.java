package lua;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

class LuaConsole {
	LuaConsole() {
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("ConPrintf", new ConPrintf());
		_g.set("ConPrintTable", new ConPrintTable());
		_g.set("ConExecute", new ConExecute());
		_g.set("ConClear", new ConClear());
	}

	private class ConPrintf extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			String[] values = new String[args.narg() - 1];
			for (int i = 2; i <= args.narg(); i++) {
				values[i - 2] = args.tojstring(i);
			}
			System.out.println(String.format(args.checkjstring(1), (Object[]) values));
			return LuaValue.NONE;
		}
	}

	private class ConPrintTable extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class ConExecute extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(args.checkjstring(1));
			return LuaValue.NONE;
		}
	}

	private class ConClear extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}
}
