package lua;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.input.BOMInputStream;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

class LuaModule {
	private Globals globals;

	LuaModule() {
	}

	void register(Globals globals) {
		this.globals = globals;
		final LuaValue _g = globals.get("_G");
		_g.set("LoadModule", new LoadModule());
		_g.set("PLoadModule", new PLoadModule());
		_g.set("PCall", new PCall());
	}

	LuaValue loadModule(String name) {
		final String fileName = name.endsWith(".lua") ? name : name + ".lua";
		try {
			BOMInputStream in = new BOMInputStream(new FileInputStream(fileName));
			return globals.load(in, name, "t", globals);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return LuaValue.NONE;
	}

	private class LoadModule extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			LuaValue chunk = loadModule(args.arg1().checkjstring());
			return chunk.invoke(args.subargs(2));
		}
	}

	private class PLoadModule extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			LuaValue chunk = loadModule(args.arg1().checkjstring());
			LuaValue error = LuaValue.NIL;
			Varargs result;
			try {
				result = chunk.invoke(args.subargs(2));
			} catch (LuaError e) {
				e.printStackTrace();
				error = valueOf(e.getMessage());
				result = LuaValue.NIL;
			}
			return varargsOf(error, result);
		}
	}

	private class PCall extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			LuaValue chunk = args.arg1();
			LuaValue error = LuaValue.NIL;
			Varargs result;
			try {
				result = chunk.invoke(args.subargs(2));
			} catch (LuaError e) {
				e.printStackTrace();
				error = valueOf(e.getMessage());
				result = LuaValue.NIL;
			}
			return varargsOf(error, result);
		}
	}
}
