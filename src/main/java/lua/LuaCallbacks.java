package lua;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

class LuaCallbacks {
	LuaValue mainObject = null;

	LuaCallbacks() {
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("SetCallback", new SetCallback());
		_g.set("GetCallback", new GetCallback());
		_g.set("SetMainObject", new SetMainObject());
	}

	private class SetCallback extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class GetCallback extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class SetMainObject extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			mainObject = args.arg(1);
			return LuaValue.NONE;
		}
	}
}
