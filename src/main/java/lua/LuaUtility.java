package lua;

import java.awt.Desktop;
import java.net.URI;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

class LuaUtility {
	private final long startTime;

	public LuaUtility() {
		startTime = System.currentTimeMillis();
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("Deflate", new Deflate());
		_g.set("Inflate", new Inflate());
		_g.set("GetTime", new GetTime());
		_g.set("SpawnProcess", new SpawnProcess());
		_g.set("OpenURL", new OpenURL());
		_g.set("SetProfiling", new SetProfiling());
		_g.set("Restart", new Restart());
		_g.set("Exit", new Exit());
	}

	private class Deflate extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class Inflate extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class GetTime extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			final long current = System.currentTimeMillis() - startTime;
			return valueOf(current);
		}
	}

	private class SpawnProcess extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class OpenURL extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			String url = args.checkjstring(1);
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (Exception e) {
				System.err.println(this.getClass().getName() + " " + args);
				e.printStackTrace();
			}
			return LuaValue.NONE;
		}
	}

	private class SetProfiling extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class Restart extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}

	private class Exit extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.out.println(this.getClass().getName() + " " + args);
			return LuaValue.NONE;
		}
	}
}
