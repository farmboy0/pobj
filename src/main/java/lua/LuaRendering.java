package lua;

import io.vavr.Tuple2;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

import data.Alignment;
import data.FontType;
import data.ImageHandle;
import ui.DrawingBoard;

class LuaRendering {
	private DrawingBoard board;

	LuaRendering(DrawingBoard board) {
		this.board = board;
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("RenderInit", new RenderInit());
		_g.set("GetScreenSize", new GetScreenSize());
		_g.set("SetDrawLayer", new SetDrawLayer());
		_g.set("SetViewport", new SetViewport());
		//		_g.set("SetClearColor", new SetClearColor());
		_g.set("SetDrawColor", new SetDrawColor());
		_g.set("DrawImage", new DrawImage());
		_g.set("DrawImageQuad", new DrawImageQuad());
		_g.set("DrawString", new DrawString());
		_g.set("DrawStringWidth", new DrawStringWidth());
		_g.set("DrawStringCursorIndex", new DrawStringCursorIndex());
		_g.set("StripEscapes", new StripEscapes());
		//		_g.set("GetAsyncCount", new GetAsyncCount());
	}

	private class RenderInit extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			board.initRendering();
			return LuaValue.NONE;
		}
	}

	private class GetScreenSize extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			final Tuple2<Integer, Integer> d = board.getScreenSize();
			return varargsOf(valueOf(d._1()), valueOf(d._2()));
		}
	}

	private class SetDrawLayer extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			Integer layer = args.isnil(1) ? null : args.toint(1);
			Integer sublayer = args.isnil(2) ? null : args.toint(2);
			board.setDrawLayer(layer, sublayer);
			return LuaValue.NONE;
		}
	}

	private class SetViewport extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			if (!args.isnil(1)) {
				board.setViewport(args.toint(1), args.toint(2), args.toint(3), args.toint(4));
			} else {
				Tuple2<Integer, Integer> size = board.getScreenSize();
				board.setViewport(0, 0, size._1, size._2);
			}
			return LuaValue.NONE;
		}
	}

	private class SetDrawColor extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			if (args.narg() == 1) {
				board.setDrawColor(args.checkjstring(1));
			} else {
				float r = args.tofloat(1);
				float g = args.tofloat(2);
				float b = args.tofloat(3);
				float a = args.isnil(4) ? 1.0f : args.tofloat(4);
				board.setDrawColor(r, g, b, a);
			}
			return LuaValue.NONE;
		}
	}

	private class DrawImage extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			float x = args.tofloat(2);
			float y = args.tofloat(3);
			float width = args.tofloat(4);
			float height = args.tofloat(5);
			if (args.isnil(1)) {
				board.drawColor(x, y, width, height);
			} else {
				ImageHandle img = (ImageHandle) args.checkuserdata(1, ImageHandle.class);
				float u1 = args.isnil(6) ? 0.0f : args.tofloat(6);
				float v1 = args.isnil(7) ? 0.0f : args.tofloat(7);
				float u2 = args.isnil(8) ? 1.0f : args.tofloat(8);
				float v2 = args.isnil(9) ? 1.0f : args.tofloat(9);
				board.drawImage(img, x, y, width, height, u1, v1, u2, v2);
			}
			return LuaValue.NONE;
		}
	}

	private class DrawImageQuad extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			float x1 = args.tofloat(2);
			float y1 = args.tofloat(3);
			float x2 = args.tofloat(4);
			float y2 = args.tofloat(5);
			float x3 = args.tofloat(6);
			float y3 = args.tofloat(7);
			float x4 = args.tofloat(8);
			float y4 = args.tofloat(9);
			if (args.isnil(1)) {
				board.drawColor(x1, y1, x2, y2, x3, y3, x4, y4);
			} else {
				ImageHandle img = (ImageHandle) args.checkuserdata(1, ImageHandle.class);
				float u1 = args.isnil(10) ? 0.0f : args.tofloat(10);
				float v1 = args.isnil(11) ? 0.0f : args.tofloat(11);
				float u2 = args.isnil(12) ? 1.0f : args.tofloat(12);
				float v2 = args.isnil(13) ? 0.0f : args.tofloat(13);
				float u3 = args.isnil(14) ? 1.0f : args.tofloat(14);
				float v3 = args.isnil(15) ? 1.0f : args.tofloat(15);
				float u4 = args.isnil(16) ? 0.0f : args.tofloat(16);
				float v4 = args.isnil(17) ? 1.0f : args.tofloat(17);
				board.drawImage(img, x1, y1, x2, y2, x3, y3, x4, y4, u1, v1, u2, v2, u3, v3, u4, v4);
			}
			return LuaValue.NONE;
		}
	}

	private class DrawString extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			Alignment align = args.isnil(3) ? Alignment.LEFT : Alignment.valueOf(args.checkjstring(3));
			FontType font = FontType.toFontType(args.checkjstring(5));
			board.drawString(args.tofloat(1), args.tofloat(2), args.checkint(4), align, font, args.checkjstring(6));
			return LuaValue.NONE;
		}
	}

	private class DrawStringWidth extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			int height = args.toint(1);
			FontType font = FontType.toFontType(args.checkjstring(2));
			String text = args.checkjstring(3);
			int width = board.drawStringWidth(height, font, text);
			return valueOf(width);
		}
	}

	private class DrawStringCursorIndex extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			int height = args.toint(1);
			FontType font = FontType.toFontType(args.checkjstring(2));
			String text = args.tojstring(3);
			int width = board.drawStringCursorIndex(height, font, text, args.checkint(4), args.checkint(5));
			return valueOf(width);
		}
	}

	private class StripEscapes extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return valueOf(board.stripColorCodes(args.checkjstring(1)));
		}
	}
}
