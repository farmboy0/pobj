package lua;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

class LuaPaths {
	LuaPaths() {
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("GetScriptPath", new GetScriptPath());
		_g.set("GetRuntimePath", new GetRuntimePath());
		_g.set("GetUserPath", new GetUserPath());
		_g.set("MakeDir", new MakeDir());
		_g.set("RemoveDir", new RemoveDir());
		_g.set("SetWorkDir", new SetWorkDir());
		_g.set("GetWorkDir", new GetWorkDir());
	}

	private class GetScriptPath extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return valueOf(System.getProperty("user.home"));
		}
	}

	private class GetRuntimePath extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class GetUserPath extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class MakeDir extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return valueOf(true);
		}
	}

	private class RemoveDir extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class SetWorkDir extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class GetWorkDir extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}
}
