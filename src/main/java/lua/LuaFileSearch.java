package lua;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicInteger;

import io.vavr.collection.List;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

import data.FileSearchHandle;

class LuaFileSearch {
	private AtomicInteger handleId;
	private LuaTable metatable;

	public LuaFileSearch() {
		handleId = new AtomicInteger(1);
	}

	void register(Globals globals) {
		final LuaValue _g = globals.get("_G");
		_g.set("NewFileSearch", new NewFileSearch());

		final LuaTable search = new LuaTable();
		search.set("NextFile", new NextFile());
		search.set("GetFileName", new GetFileName());
		search.set("GetFileSize", new GetFileSize());
		search.set("GetFileModifiedTime", new GetFileModifiedTime());

		metatable = new LuaTable();
		metatable.set(LuaValue.INDEX, search);
		metatable.set("__gc", new GarbageCollector());
	}

	private class NewFileSearch extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			final String search = args.checkjstring(1);
			final String searchdir = search.substring(0, search.lastIndexOf('/'));
			final String searchfilter = search.substring(search.lastIndexOf('/') + 1);
			final boolean searchforfiles = searchfilter.contains(".");

			File dir = new File(searchdir);
			if (!dir.isDirectory()) {
				return LuaValue.NIL;
			}

			try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(dir.toPath(), searchfilter)) {
				final List<File> files = List.ofAll(dirStream::iterator).map(Path::toFile).filter(f -> f.isFile() == searchforfiles);
				if (files.isEmpty()) {
					return LuaValue.NIL;
				}
				final FileSearchHandle h = new FileSearchHandle();
				h.id = handleId.getAndAdd(1);
				h.fileSearch = search;
				h.files = files;
				h.currentIndex = 0;
				return LuaValue.userdataOf(h, metatable);
			} catch (IOException e) {
				throw new LuaError(e);
			}
		}
	}

	private class GarbageCollector extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			return LuaValue.NONE;
		}
	}

	private class NextFile extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			FileSearchHandle search = (FileSearchHandle) args.checkuserdata(1, FileSearchHandle.class);
			if (search.currentIndex + 1 < search.files.size()) {
				search.currentIndex++;
				return valueOf(true);
			}
			return valueOf(false);
		}
	}

	private class GetFileName extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			FileSearchHandle search = (FileSearchHandle) args.checkuserdata(1, FileSearchHandle.class);
			return LuaValue.valueOf(search.files.get(search.currentIndex).getName());
		}
	}

	private class GetFileSize extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			FileSearchHandle search = (FileSearchHandle) args.checkuserdata(1, FileSearchHandle.class);
			return LuaValue.valueOf(search.files.get(search.currentIndex).length());
		}
	}

	private class GetFileModifiedTime extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			FileSearchHandle search = (FileSearchHandle) args.checkuserdata(1, FileSearchHandle.class);
			return LuaValue.valueOf(search.files.get(search.currentIndex).lastModified());
		}
	}
}
