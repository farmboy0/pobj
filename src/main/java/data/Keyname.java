package data;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.MouseEvent;

import io.vavr.collection.Stream;
import io.vavr.control.Option;

public enum Keyname {
	LEFTBUTTON(KeyType.MOUSE, MouseEvent.BUTTON1), //
	RIGHTBUTTON(KeyType.MOUSE, MouseEvent.BUTTON2), //
	MIDDLEBUTTON(KeyType.MOUSE, MouseEvent.BUTTON3), //
	WHEELUP(KeyType.MOUSE, MouseEvent.BUTTON3), //
	WHEELDOWN(KeyType.MOUSE, MouseEvent.BUTTON3), //

	CTRL(KeyType.KEYBOARD, KeyEvent.VK_CONTROL), //
	SHIFT(KeyType.KEYBOARD, KeyEvent.VK_SHIFT), //
	ALT(KeyType.KEYBOARD, KeyEvent.VK_ALT), //

	ESCAPE(KeyType.KEYBOARD, KeyEvent.VK_ESCAPE), //
	TAB(KeyType.KEYBOARD, KeyEvent.VK_TAB), //
	RETURN(KeyType.KEYBOARD, KeyEvent.VK_ENTER), //
	BACK(KeyType.KEYBOARD, KeyEvent.VK_BACK_SPACE), //
	DELETE(KeyType.KEYBOARD, KeyEvent.VK_DELETE), //
	HOME(KeyType.KEYBOARD, KeyEvent.VK_HOME), //
	END(KeyType.KEYBOARD, KeyEvent.VK_END), //
	UP(KeyType.KEYBOARD, KeyEvent.VK_UP), //
	DOWN(KeyType.KEYBOARD, KeyEvent.VK_DOWN), //
	LEFT(KeyType.KEYBOARD, KeyEvent.VK_LEFT), //
	RIGHT(KeyType.KEYBOARD, KeyEvent.VK_RIGHT), //
	PAGEUP(KeyType.KEYBOARD, KeyEvent.VK_PAGE_UP), //
	PAGEDOWN(KeyType.KEYBOARD, KeyEvent.VK_PAGE_DOWN);

	private final KeyType type;
	private final short keySymbol;

	Keyname(KeyType type, short keySymbol) {
		this.type = type;
		this.keySymbol = keySymbol;
	}

	public static Option<Keyname> fromEvent(KeyEvent e) {
		return Stream.of(values())
			.filter(kn -> kn.type == KeyType.KEYBOARD)
			.find(kn -> kn.keySymbol == e.getKeySymbol());
	}

	public static Option<Keyname> fromEvent(MouseEvent e) {
		return Stream.of(values())
			.filter(kn -> kn.type == KeyType.MOUSE)
			.find(kn -> kn.keySymbol == e.getButton());
	}

	private enum KeyType {
		KEYBOARD, MOUSE;
	}
}
