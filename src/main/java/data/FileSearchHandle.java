package data;

import java.io.File;

import io.vavr.collection.List;

public class FileSearchHandle {
	public int id;
	public String fileSearch;
	public List<File> files;
	public int currentIndex;
}
