package data;

import java.awt.image.BufferedImage;

public final class ImageHandle {
	public int id;
	public String file;
	public ImageMode mode;
	public BufferedImage image;
}
