package data;

import java.awt.Font;

public enum FontType {
	FIXED(new Font("Bitstream Vera Mono", Font.PLAIN, 14)), //
	VAR(new Font("Liberation Sans", Font.PLAIN, 14)), //
	VAR_BOLD(new Font("Liberation Sans Bold", Font.BOLD, 14));

	private Font font;

	private FontType(Font font) {
		this.font = font;
	}

	public Font getFont() {
		return font;
	}

	public static FontType toFontType(String value) {
		if ("VAR BOLD".equals(value)) {
			return VAR_BOLD;
		}
		return valueOf(value);
	}
}
