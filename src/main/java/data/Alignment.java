package data;

import io.vavr.Function3;

public enum Alignment {
	LEFT((x, surW, strW) -> x), //
	CENTER((x, surW, strW) -> x + Math.floorDiv(surW - strW, 2)), //
	CENTER_X((x, surW, strW) -> x - Math.floorDiv(strW, 2)), //
	RIGHT((x, surW, strW) -> surW - strW - x), //
	RIGHT_X((x, surW, strW) -> x - strW + 5), //
	;

	private Function3<Integer, Integer, Integer, Integer> startComputer;

	private Alignment(Function3<Integer, Integer, Integer, Integer> startComputer) {
		this.startComputer = startComputer;
	}

	public int computeStart(int initialX, int surfaceWidth, int stringWidth) {
		return startComputer.apply(initialX, surfaceWidth, stringWidth);
	}
}
