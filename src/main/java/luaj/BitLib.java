package luaj;

import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

public class BitLib extends TwoArgFunction {
	public BitLib() {
	}

	@Override
	public LuaValue call(LuaValue modname, LuaValue env) {
		final LuaTable bit = new LuaTable();
		bit.set("tobit", new tobit());
		bit.set("tohex", new tohex());
		bit.set("bswap", new bswap());

		LuaValue bit32 = env.get("bit32");
		bit.set("bnot", bit32.get("bnot"));
		bit.set("band", bit32.get("band"));
		bit.set("bor", bit32.get("bor"));
		bit.set("bxor", bit32.get("bxor"));
		bit.set("lshift", bit32.get("lshift"));
		bit.set("rshift", bit32.get("rshift"));
		bit.set("arshift", bit32.get("arshift"));
		bit.set("rol", bit32.get("lrotate"));
		bit.set("ror", bit32.get("rrotate"));

		env.set("bit", bit);
		env.get("package")
			.get("loaded")
			.set("bit", bit);

		return bit;
	}

	private static final class tobit extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.err.println("tobit");
			return LuaValue.NONE;
		}
	}

	private static final class tohex extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.err.println("tohex");
			return LuaValue.NONE;
		}
	}

	private static final class bswap extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.err.println("bswap");
			return LuaValue.NONE;
		}
	}
}
