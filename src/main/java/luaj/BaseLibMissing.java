package luaj;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

public class BaseLibMissing extends TwoArgFunction {
	private Globals globals;

	@Override
	public LuaValue call(LuaValue modname, LuaValue env) {
		globals = env.checkglobals();
		env.set("getfenv", new getfenv());
		env.set("loadstring", new loadstring());
		env.set("setfenv", new setfenv());
		env.set("unpack", env.get("table")
			.get("unpack"));
		return env;
	}

	private class getfenv extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.err.println("getfenv");
			return LuaValue.NONE;
		}
	}

	private class loadstring extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			String script = args.checkjstring(1);
			LuaValue chunkName = args.arg(2);
			LuaValue chunk;
			if (chunkName.isnil() || !chunkName.isstring()) {
				chunk = globals.load(script);
			} else {
				chunk = globals.load(script, chunkName.tojstring());
			}
			return chunk;
		}
	}

	private class setfenv extends VarArgFunction {
		@Override
		public Varargs invoke(Varargs args) {
			System.err.println("setfenv");
			return LuaValue.NONE;
		}
	}
}
